package ru.t1.bugakov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected Socket socket;

    public AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

}
