package ru.t1.bugakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.bugakov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.bugakov.tm.api.repository.ICommandRepository;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.*;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.dto.request.ServerAboutRequest;
import ru.t1.bugakov.tm.dto.request.ServerVersionRequest;
import ru.t1.bugakov.tm.endpoint.SystemEndpoint;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.bugakov.tm.exception.system.CommandNotSupportedException;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.repository.CommandRepository;
import ru.t1.bugakov.tm.repository.ProjectRepository;
import ru.t1.bugakov.tm.repository.TaskRepository;
import ru.t1.bugakov.tm.repository.UserRepository;
import ru.t1.bugakov.tm.service.*;
import ru.t1.bugakov.tm.util.SystemUtil;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.bugakov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDemoData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(admin.getId(), "p111", "222");
        taskService.create(admin.getId(), "t001", "222");
        taskService.create(admin.getId(), "t002", "444");

        @NotNull final User test = userService.create("test", "test", "test@tast.ru");
        projectService.create(test.getId(), "p333", "444");
        taskService.create(test.getId(), "t003", "222");
        taskService.create(test.getId(), "t004", "444");

        userService.create("user", "user", "user@user.ru");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        backup.start();
        fileScanner.start();
        server.start();
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("TASK MANAGER IS SHUTTING DOWN");
        backup.stop();
        fileScanner.stop();
        server.stop();
    }

    public void start(@NotNull final String[] args) {
        processArguments(args);
        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command, true);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void processCommand(@NotNull final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

}