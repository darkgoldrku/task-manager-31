package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.TaskSort;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final String userId = getAuthService().getUserId();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull List<Task> tasks;
        if (sort != null) {
            tasks = getTaskService().findAll(userId, sort.getComparator());
        } else {
            tasks = getTaskService().findAll(userId);
        }
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

}
