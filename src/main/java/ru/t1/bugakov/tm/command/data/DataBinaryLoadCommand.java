package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        fileInputStream.close();
        objectInputStream.close();
        setDomain(domain);
    }

    @Override
    public @NotNull
    String getName() {
        return NAME;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from binary file";
    }

}
