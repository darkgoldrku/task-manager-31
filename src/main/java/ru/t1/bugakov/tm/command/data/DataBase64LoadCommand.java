package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Date = new String(base64Byte);
        final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Date);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        byteArrayInputStream.close();
        objectInputStream.close();
        setDomain(domain);
    }

    @Override
    public @NotNull
    String getName() {
        return NAME;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from BASE64 file";
    }

}
