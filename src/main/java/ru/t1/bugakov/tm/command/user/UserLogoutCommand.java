package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Logout user.";
    }

}
